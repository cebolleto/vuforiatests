﻿using UnityEngine;
using System.Collections;

public class fakeShadow : MonoBehaviour {
	public Transform target;

	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(target.position.x, 0.0f, target.position.z);
	}
}
