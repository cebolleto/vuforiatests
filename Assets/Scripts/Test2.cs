﻿using UnityEngine;
using System.Collections;

public class Test2 : MonoBehaviour {

	public SkinnedMeshRenderer mesh;
	public UnityEngine.UI.Dropdown dropdown;
	public float morphSpeed = 0.01f;
	
	void Update () {
		int morphSelected = dropdown.value - 1;
		for(int i = 0; i < 3; ++i) {
			float increment = (i == morphSelected) ? 1.0f : -1.0f;

			mesh.SetBlendShapeWeight(i, Mathf.Clamp(mesh.GetBlendShapeWeight(i) + increment * Time.deltaTime * morphSpeed, 0.0f, 100.0f));
		}
	}

	public void OnOptionChanged(string n) {
		switch(dropdown.value) {
			case 0:
				break;
		}
	}
}
