﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public AudioClip bounce;
	public AudioSource audioSource;
	public Rigidbody rigidbody;

	// Use this for initialization
	void OnCollisionEnter(Collision collision) {
		if(!audioSource.isPlaying && rigidbody.velocity.magnitude > 1.0f)
			audioSource.Play();
	}
}
