﻿using UnityEngine;
using System.Collections;

public class ProjectorInitializer : MonoBehaviour {

	public Projector projector;
	public Material projectorMaterial;

	// Use this for initialization
	void Start () {
		projector.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 180.0f));

		Vuforia.VuforiaBehaviour.Instance.RegisterBackgroundTextureChangedCallback(
			() => {
				projectorMaterial.SetTexture("_ShadowTex", Vuforia.VuforiaRenderer.Instance.VideoBackgroundTexture);
				projector.fieldOfView = Vuforia.CameraDevice.Instance.GetCameraFieldOfViewRads().y * Mathf.Rad2Deg;
				
				Vuforia.VuforiaRenderer.VideoTextureInfo textInfo = Vuforia.VuforiaRenderer.Instance.GetVideoTextureInfo();
				
				projector.aspectRatio = textInfo.imageSize.x / (float)textInfo.imageSize.y;
				projectorMaterial.SetFloat("_CutX", textInfo.imageSize.x / (float)textInfo.textureSize.x);
				projectorMaterial.SetFloat("_CutY", textInfo.imageSize.y / (float)textInfo.textureSize.y);
			}
		);
	}
}
