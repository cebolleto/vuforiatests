﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreCounter : MonoBehaviour {

	private int score;
	public Text text;

	void Start() {
		score = 0;
		text.text = "" + score;
	}

	// Use this for initialization
	void OnTriggerEnter() {
		score ++;
		text.text = "" + score;
	}
}
