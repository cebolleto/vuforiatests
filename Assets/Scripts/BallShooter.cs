﻿using UnityEngine;
using System.Collections;

public class BallShooter : MonoBehaviour {

	public Rigidbody ball;
	public float force = 1.0f;

	// Use this for initialization
	void Start () {
		Input.simulateMouseWithTouches = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)) {
			ball.isKinematic = true; //Reset all forces
			ball.transform.position = transform.position;

			ball.isKinematic = false;
			ball.AddForce(transform.forward * force);
		}
	}
}
